package com.groupeisi.springbootgitlabcicd.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {
    @GetMapping("/hellocicd")
    public ResponseEntity<String> welcome(){
        return ResponseEntity.ok("welcome to our gitlab ci/cd");
    }
}
