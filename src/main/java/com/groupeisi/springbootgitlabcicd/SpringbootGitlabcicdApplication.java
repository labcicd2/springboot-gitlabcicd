package com.groupeisi.springbootgitlabcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGitlabcicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootGitlabcicdApplication.class, args);
	}

}
