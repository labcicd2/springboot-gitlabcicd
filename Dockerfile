FROM openjdk:17-jdk-slim

LABEL maintainer = "Boubacar Siddy DIALLO boubasiddy00@gmail.com"

ADD target/springboot-gitlabcicd-0.0.1.jar springboot-gitlabcicd.jar

ENTRYPOINT ["java", "-jar", "springboot-gitlabcicd.jar"]